// -- Create Bucket for CloudTrail + Config Bucket Access Logs
resource "aws_s3_bucket" "log_bucket" {
  bucket = var.s3_access_log_bucket_name
  acl    = "log-delivery-write"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_public_access_block" "log_bucket" {
  bucket = aws_s3_bucket.log_bucket.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_policy" "log_bucket_https" {
  depends_on = [aws_s3_bucket_public_access_block.log_bucket]

  bucket = aws_s3_bucket.log_bucket.id
  policy = templatefile("${path.module}/templates/s3_https_only.tpl", {
    S3_BUCKET_ARN = aws_s3_bucket.log_bucket.arn
  })
}
