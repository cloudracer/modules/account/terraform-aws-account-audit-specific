resource "aws_sns_topic" "audit" {
  name = var.aws_sns_topic_name

  kms_master_key_id = aws_kms_key.cloudtrail.arn

  tags = local.tags
}

resource "aws_sns_topic_policy" "audit" {
  arn = aws_sns_topic.audit.arn
  policy = templatefile("${path.module}/templates/sns_policy.tpl", {
    SNS_ARN     = aws_sns_topic.audit.arn,
    ACCOUNT_IDS = join(",", var.organization_account_ids)
  })
}

resource "aws_sns_topic_subscription" "audit" {
  for_each = toset(var.audit_email_subscriptions)

  topic_arn = aws_sns_topic.audit.arn
  protocol  = "email"
  endpoint  = each.value
}
