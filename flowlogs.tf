# KMS Key
resource "aws_kms_key" "vpc_flowlogs" {
  description             = "CloudTrail encryption key"
  deletion_window_in_days = 7
  policy = templatefile("${path.module}/templates/kms_key_policy.tpl", {
    AUDIT_ACCOUNT_ID = data.aws_caller_identity.current.account_id
    ROOT_ACCOUNT_ID  = data.aws_caller_identity.root.account_id
  })
  enable_key_rotation = true

  tags = local.tags
}

resource "aws_kms_alias" "vpc_flowlogs" {
  name          = "alias/flowlogs"
  target_key_id = aws_kms_key.vpc_flowlogs.key_id
}

// -- Create Bucket for VPC Flow Logs
resource "aws_s3_bucket" "vpc_flowlogs" {
  bucket        = var.vpc_flowlog_bucket_name
  force_destroy = true
  policy = templatefile("${path.module}/templates/vpc_flow_logs_bucket_policy.tpl", {
    VPC_FLOWLOGS_BUCKET_NAME = var.vpc_flowlog_bucket_name,
    NETWORK_ACCOUNT_ID       = local.vpc_flowlog_network_account_id
  })

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.vpc_flowlogs.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  logging {
    target_bucket = aws_s3_bucket.log_bucket.id
    target_prefix = "vpc_flowlogs/"
  }

  tags = local.tags
}

resource "aws_s3_bucket_public_access_block" "vpc_flowlogs" {
  bucket                  = aws_s3_bucket.vpc_flowlogs.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
