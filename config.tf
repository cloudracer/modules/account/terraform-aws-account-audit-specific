resource "aws_s3_bucket" "config" {
  bucket        = var.aws_config_bucket_name
  force_destroy = true
  policy = templatefile("${path.module}/templates/config_bucket_policy.tpl", {
    CONFIG_BUCKET_NAME = var.aws_config_bucket_name,
    ACCOUNT_LIST       = var.aws_config_accounts
  })

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  logging {
    target_bucket = aws_s3_bucket.log_bucket.id
    target_prefix = "config/"
  }

  tags = local.tags
}

resource "aws_s3_bucket_public_access_block" "config" {
  bucket = aws_s3_bucket.config.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_config_configuration_aggregator" "organization" {
  name = "config-aggregator"

  account_aggregation_source {
    account_ids = var.aws_config_accounts
    all_regions = true
  }
}
