data "aws_caller_identity" "current" {
  provider = aws
}

data "aws_region" "current" {
  provider = aws
}

data "aws_caller_identity" "network" {
  provider = aws.network
}

data "aws_caller_identity" "root" {
  provider = aws.root
}
