# -----------------------------------------------------------
# Managed Config rules without parameters
# -----------------------------------------------------------

resource "aws_config_organization_managed_rule" "config_rule_without_params" {
  for_each = toset(var.config_rules_without_params)
  provider = aws.root

  name            = each.value
  rule_identifier = each.value

}


# -----------------------------------------------------------
# Managed Config rules with parameters
# -----------------------------------------------------------

resource "aws_config_organization_managed_rule" "config_rule_with_params" {
  for_each = var.config_rules_with_params
  provider = aws.root

  name             = each.value.identifier
  rule_identifier  = each.value.identifier
  input_parameters = each.value["input_parameters"]

}
