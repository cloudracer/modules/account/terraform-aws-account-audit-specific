resource "aws_s3_bucket" "gd_bucket" {
  bucket        = var.guardduty_bucket_name
  acl           = "private"
  force_destroy = true
  policy = templatefile("${path.module}/templates/guardduty_bucket_policy.tpl", {
    GUARDDUTY_BUCKET_NAME = var.guardduty_bucket_name
  })

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  logging {
    target_bucket = aws_s3_bucket.log_bucket.id
    target_prefix = "guardduty/"
  }

  tags = local.tags
}

resource "aws_s3_bucket_public_access_block" "gd" {
  bucket                  = aws_s3_bucket.gd_bucket.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_kms_key" "gd_key" {
  description             = "GuardDuty encryption key"
  deletion_window_in_days = 7
  policy                  = data.aws_iam_policy_document.kms_pol.json

  enable_key_rotation = true
  tags                = local.tags
}

resource "aws_kms_alias" "gd" {
  name          = "alias/guardduty"
  target_key_id = aws_kms_key.gd_key.key_id
}
