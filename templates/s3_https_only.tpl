{
    "Id": "AllowEncryptedRequestsOnly",
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowEncryptedRequestsOnly",
            "Action": "s3:*",
            "Effect": "Deny",
            "Resource": [
                "${S3_BUCKET_ARN}",
                "${S3_BUCKET_ARN}/*"
            ],
            "Condition": {
                "Bool": {
                     "aws:SecureTransport": "false"
                }
            },
           "Principal": "*"
        }
    ]
}
