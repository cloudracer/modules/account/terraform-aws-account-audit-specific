{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSConfigBucketPermissionsCheck",
            "Effect": "Allow",
            "Principal": {
                "Service": [
                    "config.amazonaws.com"
                ]
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "arn:aws:s3:::${CONFIG_BUCKET_NAME}"
        },
        {
            "Sid": "AWSConfigBucketExistenceCheck",
            "Effect": "Allow",
            "Principal": {
                "Service": [
                    "config.amazonaws.com"
                ]
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${CONFIG_BUCKET_NAME}"
        },
        {
            "Sid": "AWSConfigBucketDelivery",
            "Effect": "Allow",
            "Principal": {
                "Service": [
                    "config.amazonaws.com"
                ]
            },
            "Action": "s3:PutObject",
            "Resource": [
                %{ for account in ACCOUNT_LIST ~}
                    "arn:aws:s3:::${CONFIG_BUCKET_NAME}/AWSLogs/${account}/*"%{ if index(ACCOUNT_LIST, account)+1 < length(ACCOUNT_LIST) },%{ endif }
                %{ endfor ~}
            ],
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        },
        {
            "Sid": "AllowEncryptedRequestsOnly",
            "Action": "s3:*",
            "Effect": "Deny",
            "Resource": [
                "arn:aws:s3:::${CONFIG_BUCKET_NAME}",
                "arn:aws:s3:::${CONFIG_BUCKET_NAME}/*"
            ],
            "Condition": {
                "Bool": {
                    "aws:SecureTransport": "false"
                }
            },
            "Principal": "*"
        }
    ]
}
