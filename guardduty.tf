resource "aws_guardduty_detector" "audit" {
  enable = true
  tags   = local.tags
}

resource "aws_guardduty_publishing_destination" "gd" {
  detector_id     = aws_guardduty_detector.audit.id
  destination_arn = aws_s3_bucket.gd_bucket.arn
  kms_key_arn     = aws_kms_key.gd_key.arn
}
