# KMS Key
resource "aws_kms_key" "cloudtrail" {
  description             = "CloudTrail encryption key"
  deletion_window_in_days = 7
  policy = templatefile("${path.module}/templates/kms_key_policy.tpl", {
    AUDIT_ACCOUNT_ID = data.aws_caller_identity.current.account_id
    ROOT_ACCOUNT_ID  = data.aws_caller_identity.root.account_id
  })
  enable_key_rotation = true

  tags = local.tags
}

resource "aws_kms_alias" "cloudtrail" {
  name          = "alias/cloudtrail"
  target_key_id = aws_kms_key.cloudtrail.key_id
}

resource "aws_s3_bucket" "cloudtrail" {
  bucket        = var.cloudtrail_bucket_name
  force_destroy = true
  policy = templatefile("${path.module}/templates/cloudtrail_bucket_policy.tpl", {
    CLOUDTRAIL_BUCKET_NAME = var.cloudtrail_bucket_name
  })

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.cloudtrail.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  logging {
    target_bucket = aws_s3_bucket.log_bucket.id
    target_prefix = "cloudtrail/"
  }

  versioning {
    enabled    = true
    mfa_delete = false
  }

  tags = local.tags
}

resource "aws_s3_bucket_public_access_block" "cloudtrail" {
  bucket                  = aws_s3_bucket.cloudtrail.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
