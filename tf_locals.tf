locals {
  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-account-audit-specific"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/terraform-aws-account-audit-specific"
    Stage                   = var.stage
    System                  = var.system
  }
  tags = merge(local.tags_module, var.tags)

  vpc_flowlog_network_account_id = data.aws_caller_identity.network.account_id
  aws_route53_network_account_id = data.aws_caller_identity.network.account_id
}
