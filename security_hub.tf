// Enable Security Hub in Root
resource "aws_securityhub_account" "main" {
  provider = aws.root
  count    = var.enable_security_hub ? 1 : 0
}

// Choose the Audit Account as Security Hub Admin
resource "aws_securityhub_organization_admin_account" "main" {
  provider   = aws.root
  count      = var.enable_security_hub ? 1 : 0
  depends_on = [aws_securityhub_account.main]

  admin_account_id = data.aws_caller_identity.current.account_id
}
