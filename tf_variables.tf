// Required for provider
variable "region" {
  type        = string
  description = "The region of the account"
}

variable "aws_organizations_account_resource" {
  type        = map(any)
  description = "The complete resource that holds all information about the created aws_organization_account(s)"
}

// Organization specific tags
variable "organization" {
  type        = string
  description = "Complete name of the organisation"
}

variable "system" {
  type        = string
  description = "Name of a dedicated system or application"
}

variable "stage" {
  type        = string
  description = "Name of a dedicated system or application"
}

// Tags
variable "tags" {
  type        = map(string)
  description = "Tag that should be applied to all resources."
  default     = {}
}

// Module specific variables
variable "cloudtrail_bucket_name" {
  type        = string
  description = "Name for the CloudTrail S3 Bucket"
}

variable "guardduty_bucket_name" {
  type        = string
  description = "Name for the GuardDuty S3 Bucket"
}

variable "aws_config_bucket_name" {
  type        = string
  description = "Name for the AWS Config S3 Bucket"
}

variable "aws_route53_bucket_name" {
  type        = string
  description = "Name for the AWS Route53 S3 Bucket"
}

variable "aws_config_accounts" {
  type        = list(string)
  description = "Map of all AWS Account IDs that shall be aggregated in the audit account"
}

variable "s3_access_log_bucket_name" {
  type        = string
  description = "Name for the Logging S3 Bucket (for CloudTrail & Config Buckets)"
}

variable "vpc_flowlog_bucket_name" {
  type        = string
  description = "Name for the VPC Flow Logs S3 Bucket"
}

variable "aws_sns_topic_name" {
  type        = string
  description = "Name for the Audit SNS Topic"
}

variable "enable_security_hub" {
  type        = bool
  description = "If AWS SecurityHub should be activated or not"
}

variable "organization_account_ids" {}

// Config
variable "config_rules_without_params" {
  description = "Config rules without parameters"
  type        = list(string)
}

variable "config_rules_with_params" {
  description = "Config rules with parameters"
  type        = any
}

variable "audit_email_subscriptions" {
  type        = list(string)
  description = "A list of emails that want to subscribe the Audit SNS Topic"
  default     = []
}
