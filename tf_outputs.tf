output "audit_sns_topic" {
  value = aws_sns_topic.audit.arn
}

output "guardduty_detector_id" {
  value = aws_guardduty_detector.audit.id
}

output "aws_config_bucket_name" {
  value = aws_s3_bucket.config.bucket
}

output "aws_config_bucket_arn" {
  value = aws_s3_bucket.config.arn
}

output "aws_route53_bucket_arn" {
  value = aws_s3_bucket.route53.arn
}

output "aws_cloudtrail_bucket_name" {
  value = aws_s3_bucket.cloudtrail.bucket
}

output "aws_cloudtrail_bucket_arn" {
  value = aws_s3_bucket.cloudtrail.arn
}

output "aws_cloudtrail_kms_key" {
  value = aws_kms_key.cloudtrail
}

output "aws_flowlogs_bucket_name" {
  value = aws_s3_bucket.vpc_flowlogs.bucket
}

output "aws_flowlogs_bucket_arn" {
  value = aws_s3_bucket.vpc_flowlogs.arn
}
