# KMS Key
resource "aws_kms_key" "route53" {
  description             = "CloudTrail encryption key"
  deletion_window_in_days = 7
  policy = templatefile("${path.module}/templates/kms_key_policy.tpl", {
    AUDIT_ACCOUNT_ID = data.aws_caller_identity.current.account_id
    ROOT_ACCOUNT_ID  = data.aws_caller_identity.root.account_id
  })
  enable_key_rotation = true

  tags = local.tags
}

resource "aws_kms_alias" "route53" {
  name          = "alias/route53"
  target_key_id = aws_kms_key.route53.key_id
}

// -- Create Bucket for Route53 Logs
resource "aws_s3_bucket" "route53" {
  bucket        = var.aws_route53_bucket_name
  force_destroy = true
  policy = templatefile("${path.module}/templates/route53_logs_bucket_policy.tpl", {
    AWS_ROUTE53_BUCKET_NAME = var.aws_route53_bucket_name,
    NETWORK_ACCOUNT_ID      = local.aws_route53_network_account_id
  })

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.route53.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  logging {
    target_bucket = aws_s3_bucket.log_bucket.id
    target_prefix = "route53/"
  }

  tags = local.tags
}

resource "aws_s3_bucket_public_access_block" "route53" {
  bucket                  = aws_s3_bucket.route53.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
