# Audit Account Module

This modules creates resources that are specific to the audit account:

* S3 bucket to capture CloudTrail logs to
* S3 bucket to capture AWS Config configuration snapshots
* AWS Config Aggregator that allows to browse AWS config data from all accounts in this one
* SNS messages which are published by cloudtrail
* SQS queue for receiving messages from SNS for graylog polling
* CloudWatch Log Groups for VPC Flow Logs

## Default Config Rules

```
config_rules_without_params = [
  "ALB_HTTP_DROP_INVALID_HEADER_ENABLED",
  "ALB_WAF_ENABLED",
  "API_GW_CACHE_ENABLED_AND_ENCRYPTED",
  "API_GW_EXECUTION_LOGGING_ENABLED",
  "AUTOSCALING_GROUP_ELB_HEALTHCHECK_REQUIRED",
  "CLOUD_TRAIL_CLOUD_WATCH_LOGS_ENABLED",
  "CLOUD_TRAIL_ENABLED",
  "CLOUD_TRAIL_ENCRYPTION_ENABLED",
  "CLOUD_TRAIL_LOG_FILE_VALIDATION_ENABLED",
  "CLOUDWATCH_LOG_GROUP_ENCRYPTED",
  "CMK_BACKING_KEY_ROTATION_ENABLED",
  "CODEBUILD_PROJECT_ENVVAR_AWSCRED_CHECK",
  "CW_LOGGROUP_RETENTION_PERIOD_CHECK",
  "DB_INSTANCE_BACKUP_ENABLED",
  "DMS_REPLICATION_NOT_PUBLIC",
  "EBS_IN_BACKUP_PLAN",
  "EFS_IN_BACKUP_PLAN",
  "EBS_SNAPSHOT_PUBLIC_RESTORABLE_CHECK",
  "EC2_INSTANCE_DETAILED_MONITORING_ENABLED",
  "EC2_INSTANCE_NO_PUBLIC_IP",
  "EC2_SECURITY_GROUP_ATTACHED_TO_ENI",
  "EC2_STOPPED_INSTANCE",
  "EFS_ENCRYPTED_CHECK",
  "EIP_ATTACHED",
  "ELASTICSEARCH_ENCRYPTED_AT_REST",
  "ELASTICSEARCH_IN_VPC_ONLY",
  "ELASTICACHE_REDIS_CLUSTER_AUTOMATIC_BACKUP_CHECK",
  "EC2_IMDSV2_CHECK",
  "ELASTICSEARCH_NODE_TO_NODE_ENCRYPTION_CHECK",
  "ELB_DELETION_PROTECTION_ENABLED",
  "ELB_LOGGING_ENABLED",
  "IAM_POLICY_NO_STATEMENTS_WITH_ADMIN_ACCESS",
  "IAM_ROOT_ACCESS_KEY_CHECK",
  "IAM_USER_MFA_ENABLED",
  "INCOMING_SSH_DISABLED",
  "KMS_CMK_NOT_SCHEDULED_FOR_DELETION",
  "LAMBDA_FUNCTION_PUBLIC_ACCESS_PROHIBITED",
  "MFA_ENABLED_FOR_IAM_CONSOLE_ACCESS",
  "MULTI_REGION_CLOUD_TRAIL_ENABLED",
  "RDS_CLUSTER_DELETION_PROTECTION_ENABLED",
  "RDS_INSTANCE_DELETION_PROTECTION_ENABLED",
  "RDS_LOGGING_ENABLED",
  "RDS_IN_BACKUP_PLAN",
  "RDS_SNAPSHOT_ENCRYPTED",
  "REDSHIFT_REQUIRE_TLS_SSL",
  "RDS_ENHANCED_MONITORING_ENABLED",
  "RDS_INSTANCE_PUBLIC_ACCESS_CHECK",
  "RDS_MULTI_AZ_SUPPORT",
  "RDS_SNAPSHOTS_PUBLIC_PROHIBITED",
  "RDS_STORAGE_ENCRYPTED",
  "REDSHIFT_CLUSTER_PUBLIC_ACCESS_CHECK",
  "ROOT_ACCOUNT_MFA_ENABLED",
  "S3_DEFAULT_ENCRYPTION_KMS",
  "SECURITYHUB_ENABLED",
  "SNS_ENCRYPTED_KMS",
  "S3_BUCKET_LOGGING_ENABLED",
  "S3_BUCKET_PUBLIC_READ_PROHIBITED",
  "S3_BUCKET_PUBLIC_WRITE_PROHIBITED",
  "S3_BUCKET_SERVER_SIDE_ENCRYPTION_ENABLED",
  "S3_BUCKET_SSL_REQUESTS_ONLY",
  "VPC_VPN_2_TUNNELS_UP",
  "WAFV2_LOGGING_ENABLED"
]

config_rules_with_params = {
  ACCESS_KEYS_ROTATED = {
    description      = "Checks whether the active access keys are rotated within the number of days specified in maxAccessKeyAge. The rule is NON_COMPLIANT if the access keys have not been rotated for more than maxAccessKeyAge number of days."
    identifier       = "ACCESS_KEYS_ROTATED"
    trigger_type     = "Periodic"
    input_parameters = <<EOF
      {
        "maxAccessKeyAge": "90"
      }
        EOF
  },
  ACM_CERTIFICATE_EXPIRATION_CHECK = {
    description      = "Checks whether ACM Certificates in your account are marked for expiration within the specified number of days. Certificates provided by ACM are automatically renewed. ACM does not automatically renew certificates that you import."
    identifier       = "ACM_CERTIFICATE_EXPIRATION_CHECK"
    trigger_type     = "Configuration changes and periodic"
    input_parameters = <<EOF
      {
        "daysToExpiration": "30"
      }
        EOF
  },
  INSTANCES_IN_VPC = {
    description      = "Checks whether your EC2 instances belong to a virtual private cloud (VPC). Optionally, you can specify the VPC ID to associate with your instances."
    identifier       = "INSTANCES_IN_VPC"
    trigger_type     = "Configuration changes"
    input_parameters = null
    # input_parameters = <<EOF
    # {
    #   "vpcId": ""
    # }
    #    EOF
  },
  EC2_VOLUME_INUSE_CHECK = {
    description      = "Checks whether EBS volumes are attached to EC2 instances. Optionally checks if EBS volumes are marked for deletion when an instance is terminated."
    identifier       = "EC2_VOLUME_INUSE_CHECK"
    trigger_type     = "Configuration changes"
    input_parameters = null
    # input_parameters = <<EOF
    # {
    #   "deleteOnTermination": ""
    # }
    #    EOF
  },
  GUARDDUTY_NON_ARCHIVED_FINDINGS = {
    description      = "Checks whether the Amazon GuardDuty has findings that are non archived. The rule is NON_COMPLIANT if Amazon GuardDuty has non archived low/medium/high severity findings older than the specified number in the daysLowSev/daysMediumSev/daysHighSev parameter."
    identifier       = "GUARDDUTY_NON_ARCHIVED_FINDINGS"
    trigger_type     = "Periodic"
    input_parameters = <<EOF
      {
        "daysLowSev": "30",
        "daysMediumSev": "7",
        "daysHighSev": "1"
      }
        EOF
  },
  IAM_USER_UNUSED_CREDENTIALS_CHECK = {
    description      = "Checks whether your AWS Identity and Access Management (IAM) users have passwords or active access keys that have not been used within the specified number of days you provided. Re-evaluating this rule within 4 hours of the first evaluation will have no effect on the results."
    identifier       = "IAM_USER_UNUSED_CREDENTIALS_CHECK"
    trigger_type     = "Periodic"
    input_parameters = null
    input_parameters = <<EOF
      {
        "maxCredentialUsageAge": "90"
      }
        EOF
  },
  REQUIRED_TAGS = {
    description      = "Checks whether your resources have the tags that you specify. For example, you can check whether your Amazon EC2 instances have the CostCenter tag. Separate multiple values with commas."
    identifier       = "REQUIRED_TAGS"
    trigger_type     = "Configuration changes"
    input_parameters = <<EOF
       {
          "tag1Key": "Repository",
          "tag2Key": "Terraform",
          "tag2Value": "true",
          "tag3Key": "Map-Migrated",
          "tag4Key": "Environment"
       }
           EOF
  },
  S3_ACCOUNT_LEVEL_PUBLIC_ACCESS_BLOCKS = {
    description      = "Checks whether your AWS Identity and Access Management (IAM) users have passwords or active access keys that have not been used within the specified number of days you provided. Re-evaluating this rule within 4 hours of the first evaluation will have no effect on the results."
    identifier       = "S3_ACCOUNT_LEVEL_PUBLIC_ACCESS_BLOCKS"
    trigger_type     = "Configuration changes"
    input_parameters = <<EOF
      {
        "IgnorePublicAcls": "true",
        "BlockPublicPolicy": "true",
        "BlockPublicAcls": "true",
        "RestrictPublicBuckets": "true"
      }
      EOF
  }
}
```

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |
| <a name="provider_aws.network"></a> [aws.network](#provider\_aws.network) | 4.2.0 |
| <a name="provider_aws.root"></a> [aws.root](#provider\_aws.root) | 4.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_config_configuration_aggregator.organization](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/config_configuration_aggregator) | resource |
| [aws_config_organization_managed_rule.config_rule_with_params](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/config_organization_managed_rule) | resource |
| [aws_config_organization_managed_rule.config_rule_without_params](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/config_organization_managed_rule) | resource |
| [aws_guardduty_detector.audit](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/guardduty_detector) | resource |
| [aws_guardduty_publishing_destination.gd](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/guardduty_publishing_destination) | resource |
| [aws_kms_alias.cloudtrail](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_alias.gd](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_alias.route53](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_alias.vpc_flowlogs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_key.cloudtrail](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_kms_key.gd_key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_kms_key.route53](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_kms_key.vpc_flowlogs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_s3_bucket.cloudtrail](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.config](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.gd_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.log_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.route53](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.vpc_flowlogs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_policy.log_bucket_https](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | resource |
| [aws_s3_bucket_public_access_block.cloudtrail](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_public_access_block.config](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_public_access_block.gd](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_public_access_block.log_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_public_access_block.route53](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_s3_bucket_public_access_block.vpc_flowlogs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [aws_securityhub_account.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/securityhub_account) | resource |
| [aws_securityhub_organization_admin_account.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/securityhub_organization_admin_account) | resource |
| [aws_sns_topic.audit](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic) | resource |
| [aws_sns_topic_policy.audit](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic_policy) | resource |
| [aws_sns_topic_subscription.audit](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sns_topic_subscription) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_caller_identity.network](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_caller_identity.root](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.kms_pol](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_audit_email_subscriptions"></a> [audit\_email\_subscriptions](#input\_audit\_email\_subscriptions) | A list of emails that want to subscribe the Audit SNS Topic | `list(string)` | `[]` | no |
| <a name="input_aws_config_accounts"></a> [aws\_config\_accounts](#input\_aws\_config\_accounts) | Map of all AWS Account IDs that shall be aggregated in the audit account | `list(string)` | n/a | yes |
| <a name="input_aws_config_bucket_name"></a> [aws\_config\_bucket\_name](#input\_aws\_config\_bucket\_name) | Name for the AWS Config S3 Bucket | `string` | n/a | yes |
| <a name="input_aws_organizations_account_resource"></a> [aws\_organizations\_account\_resource](#input\_aws\_organizations\_account\_resource) | The complete resource that holds all information about the created aws\_organization\_account(s) | `map(any)` | n/a | yes |
| <a name="input_aws_route53_bucket_name"></a> [aws\_route53\_bucket\_name](#input\_aws\_route53\_bucket\_name) | Name for the AWS Route53 S3 Bucket | `string` | n/a | yes |
| <a name="input_aws_sns_topic_name"></a> [aws\_sns\_topic\_name](#input\_aws\_sns\_topic\_name) | Name for the Audit SNS Topic | `string` | n/a | yes |
| <a name="input_cloudtrail_bucket_name"></a> [cloudtrail\_bucket\_name](#input\_cloudtrail\_bucket\_name) | Name for the CloudTrail S3 Bucket | `string` | n/a | yes |
| <a name="input_config_rules_with_params"></a> [config\_rules\_with\_params](#input\_config\_rules\_with\_params) | Config rules with parameters | `any` | n/a | yes |
| <a name="input_config_rules_without_params"></a> [config\_rules\_without\_params](#input\_config\_rules\_without\_params) | Config rules without parameters | `list(string)` | n/a | yes |
| <a name="input_enable_security_hub"></a> [enable\_security\_hub](#input\_enable\_security\_hub) | If AWS SecurityHub should be activated or not | `bool` | n/a | yes |
| <a name="input_guardduty_bucket_name"></a> [guardduty\_bucket\_name](#input\_guardduty\_bucket\_name) | Name for the GuardDuty S3 Bucket | `string` | n/a | yes |
| <a name="input_organization"></a> [organization](#input\_organization) | Complete name of the organisation | `string` | n/a | yes |
| <a name="input_organization_account_ids"></a> [organization\_account\_ids](#input\_organization\_account\_ids) | n/a | `any` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | The region of the account | `string` | n/a | yes |
| <a name="input_s3_access_log_bucket_name"></a> [s3\_access\_log\_bucket\_name](#input\_s3\_access\_log\_bucket\_name) | Name for the Logging S3 Bucket (for CloudTrail & Config Buckets) | `string` | n/a | yes |
| <a name="input_stage"></a> [stage](#input\_stage) | Name of a dedicated system or application | `string` | n/a | yes |
| <a name="input_system"></a> [system](#input\_system) | Name of a dedicated system or application | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tag that should be applied to all resources. | `map(string)` | `{}` | no |
| <a name="input_vpc_flowlog_bucket_name"></a> [vpc\_flowlog\_bucket\_name](#input\_vpc\_flowlog\_bucket\_name) | Name for the VPC Flow Logs S3 Bucket | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_audit_sns_topic"></a> [audit\_sns\_topic](#output\_audit\_sns\_topic) | n/a |
| <a name="output_aws_cloudtrail_bucket_arn"></a> [aws\_cloudtrail\_bucket\_arn](#output\_aws\_cloudtrail\_bucket\_arn) | n/a |
| <a name="output_aws_cloudtrail_bucket_name"></a> [aws\_cloudtrail\_bucket\_name](#output\_aws\_cloudtrail\_bucket\_name) | n/a |
| <a name="output_aws_cloudtrail_kms_key"></a> [aws\_cloudtrail\_kms\_key](#output\_aws\_cloudtrail\_kms\_key) | n/a |
| <a name="output_aws_config_bucket_arn"></a> [aws\_config\_bucket\_arn](#output\_aws\_config\_bucket\_arn) | n/a |
| <a name="output_aws_config_bucket_name"></a> [aws\_config\_bucket\_name](#output\_aws\_config\_bucket\_name) | n/a |
| <a name="output_aws_flowlogs_bucket_arn"></a> [aws\_flowlogs\_bucket\_arn](#output\_aws\_flowlogs\_bucket\_arn) | n/a |
| <a name="output_aws_flowlogs_bucket_name"></a> [aws\_flowlogs\_bucket\_name](#output\_aws\_flowlogs\_bucket\_name) | n/a |
| <a name="output_aws_route53_bucket_arn"></a> [aws\_route53\_bucket\_arn](#output\_aws\_route53\_bucket\_arn) | n/a |
| <a name="output_guardduty_detector_id"></a> [guardduty\_detector\_id](#output\_guardduty\_detector\_id) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
